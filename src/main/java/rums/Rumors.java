package rums;
import java.util.*;
import java.lang.*;

public class Rumors {
    static double avg_spread = 0;
    static long sessions = 0;

    static boolean run(int numberOfPeople){
        //setting up hashmap --> id of person refers to the fact if he/she is aware of the rumor
        HashMap<Integer, Boolean> references = new HashMap<Integer, Boolean>();
        references.put(0, true);
        for (int i = 1; i < numberOfPeople; i++){
            references.put(i, false);
        }

        int sender = 0, recipient = (int)(1 + Math.random() * (numberOfPeople - 1));
        int spread = 1;
        int info_from = -1;
        boolean running = true;
        while (running){
            while (recipient == sender || recipient == info_from){
                recipient = (int)(Math.random() * (numberOfPeople - 1));
            }
            if (references.get(recipient) == false) {
                references.put(recipient, true);
                spread ++;

            }
            else{
                running = false;
                break;
            }

            info_from = sender;
            sender = recipient;
            recipient = (int)(Math.random() * (numberOfPeople - 1));
        }
        avg_spread = (avg_spread * sessions + spread) / (sessions + 1);

        if (spread == numberOfPeople) return true;
        return false;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numberOfPeople = scan.nextInt();
        scan.close();

        //approximating with 5 successful sessions
        int successful_sessions = 0;
        long start = System.nanoTime();
        while (successful_sessions < 5){
            if (run(numberOfPeople)){
                successful_sessions++;
                System.out.println("successful sessions: " + successful_sessions + "/5");
            }
            sessions++;
        }
        long end = System.nanoTime();

        System.out.println(100 / (double)sessions + "%");
        System.out.println(successful_sessions + " successful sessions out from " + sessions);
        System.out.println("average spread: " + avg_spread);
        System.out.println("it took " + (double)((end - start)/Math.pow(10, 9)) + " seconds");
    }
}
